//
//  main.hpp
//  CommandLineTool
//
//  Created by Fynn Harvey on 09/10/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#ifndef main_hpp
#define main_hpp

#include <stdio.h>

class Array
{
public:
    
    Array();
    ~Array();
    void add (float itemValue);
    float get (int index);
    int size();
    
    
    

    
    

private:
    
    
    
    int arraySize;
    float *arrayPtr;
    

    
    
};


class LinkedList
{
public:
    
    
    
    LinkedList();
    ~LinkedList();
    void add (float itemValue);
    float get (int index);
    int size();
    
private:
    
    struct Node
    {
    float value;
    Node* next;
    
    };
    
    Node* current;
    Node* head;
    Node* temp;
    
};
bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
        {
        std::cout << "size is incorrect\n";
        return false;
        }
    
    for (int i = 0; i < testArraySize; i++)
        {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
            {
            std::cout << "size is incorrect\n";
            return false;
            }
        
        if (array.get (i) != testArray[i])
            {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
            }
        }
    
        //    //removing first
        //    array.remove (0);
        //    if (array.size() != testArraySize - 1)
        //    {
        //        std::cout << "with size after removing item\n";
        //        return false;
        //    }
        //
        //    for (int i = 0; i < array.size(); i++)
        //    {
        //        if (array.get(i) != testArray[i+1])
        //        {
        //            std::cout << "problems removing items\n";
        //            return false;
        //        }
        //    }
        //
        //    //removing last
        //    array.remove (array.size() - 1);
        //    if (array.size() != testArraySize - 2)
        //    {
        //        std::cout << "with size after removing item\n";
        //        return false;
        //    }
        //
        //    for (int i = 0; i < array.size(); i++)
        //    {
        //        if (array.get(i) != testArray[i + 1])
        //        {
        //            std::cout << "problems removing items\n";
        //            return false;
        //        }
        //    }
        //
        //    //remove second item
        //    array.remove (1);
        //    if (array.size() != testArraySize - 3)
        //    {
        //        std::cout << "with size after removing item\n";
        //        return false;
        //    }
        //
        //    if (array.get (0) != testArray[1])
        //    {
        //        std::cout << "problems removing items\n";
        //        return false;
        //    }
        //    
        //    if (array.get (1) != testArray[3])
        //    {
        //        std::cout << "problems removing items\n";
        //        return false;
        //    }
        //    
        //    if (array.get (2) != testArray[4])
        //    {
        //        std::cout << "problems removing items\n";
        //        return false;
        //    }
    
    return true;
}


#endif /* main_hpp */
